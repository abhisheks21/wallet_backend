package com.example.abhishek.WalletApp.controllers;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
class TestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @InjectMocks
    TestController testController;

    @Test
    @WithMockUser
    void allAccess() throws Exception{
        Assertions.assertThat(testController.allAccess()).isEqualTo("Welcome to WalletApp.");
//        mockMvc.perform(MockMvcRequestBuilders.get("/all"))
//        .andExpect(MockMvcResultMatchers.status().is(200));
    }

    @Test
    @WithMockUser
    void userAccess() {
        Assertions.assertThat(testController.userAccess()).isEqualTo("User Content.");
    }

    @Test
    void moderatorAccess() {
        Assertions.assertThat(testController.moderatorAccess()).isEqualTo("Moderator Board.");

    }

    @Test
    void adminAccess() {
        Assertions.assertThat(testController.adminAccess()).isEqualTo("Admin Board.");

    }
}