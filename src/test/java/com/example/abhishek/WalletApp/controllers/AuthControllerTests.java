package com.example.abhishek.WalletApp.controllers;

import com.example.abhishek.WalletApp.models.ERole;
import com.example.abhishek.WalletApp.models.Role;
import com.example.abhishek.WalletApp.models.User;
import com.example.abhishek.WalletApp.payload.request.LoginRequest;
import com.example.abhishek.WalletApp.payload.request.SignUpRequest;
import com.example.abhishek.WalletApp.repository.RoleRepository;
import com.example.abhishek.WalletApp.repository.UserRepository;
import com.example.abhishek.WalletApp.security.jwt.JwtUtils;
import com.example.abhishek.WalletApp.security.services.UserDetailsImpl;
import com.example.abhishek.WalletApp.security.services.UserDetailsServiceImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

//@SpringBootTest
//@WebMvcTest(controllers = AuthController.class)
@ExtendWith(MockitoExtension.class)
@ContextConfiguration
class AuthControllerTests {

    @Autowired
    AuthenticationManager authenticationManager;

    @Mock
    private UserDetailsServiceImpl userDetailsService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private PasswordEncoder encoder;

    @Mock
    SecurityContext securityContext;

    @Mock
    private JwtUtils jwtUtils;

//    @Autowired
//    private MockMvc mockMvc;

    @Mock
    Authentication authentication;

    @InjectMocks
    AuthController authController;



    @Test
    @DisplayName("Should log in")
    public void testLogin() throws Exception {
//        assertThat(4).isEqualTo(4);
//        Mockito.when();

//        LoginRequest loginRequest = new LoginRequest("Abhishek", "1234567890");
//        Authentication authentication1 = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
//                        loginRequest.getPassword()));
//        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
//        User user = new User();
//        user.setUsername("Example");
//        user.setEmail("example@example.com");
//        user.setWalletBalance(100);
//        user.setPassword("1234567890");
//        user.setRoles(set);
//
//        UserDetailsImpl expectedUserObject = new UserDetailsImpl("12",
//                user.getUsername(),
//                user.getEmail(),
//                user.getPassword(),
//                user.getWalletBalance(),
//                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
//                        .collect(Collectors.toList()));
//
//        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication1);
//        SecurityContextHolder.setContext(securityContext);
//
//        Mockito.when(SecurityContextHolder.getContext()
//                .getAuthentication()
//                .getPrincipal())
//                .thenReturn(expectedUserObject);
//
//        Mockito.when(jwtUtils.generateJwtToken(Mockito.any(Authentication.class)))
//                .thenReturn("scdcscs");
//
//        Mockito.when(authentication.getPrincipal()).thenReturn(expectedUserObject);
//        Assertions.assertThat(authController.authenticateUser(loginRequest).getStatusCodeValue())
//                .isEqualTo(200);

//        mockMvc.perform(MockMvcRequestBuilders.post("/api/auth/signin")
//                .contentType(MediaType.APPLICATION_JSON)
////                .content("{ username: Abhishek, password: 1234567890}")
//                .content(loginRequest.toString())
//                .accept(MediaType.APPLICATION_JSON))
//                .andExpect(MockMvcResultMatchers.status().is(200))
//                .andExpect(jsonPath("$.username").value("Abhishek"))
//                .andExpect(jsonPath("$.email").value("abhishek@wallet.com"));


    }
    @Test
    public void shouldNotRegisterExistingUsername(){
        Mockito.when(userRepository.existsByUsername(Mockito.any(String.class)))
                .thenReturn(true);
        SignUpRequest signUpRequest = new SignUpRequest(
                "Example",
                "example@test.com",
                Set.of("ROLE_USER"),
                "1234567890"
        );
        ResponseEntity actualResponse = authController.registerUser(signUpRequest);
        Assertions.assertThat(actualResponse.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    public void shouldNotRegisterExistingEmail(){
        Mockito.when(userRepository.existsByUsername(Mockito.any(String.class)))
                .thenReturn(false);
        Mockito.when(userRepository.existsByEmail(Mockito.any(String.class)))
                .thenReturn(true);
        SignUpRequest signUpRequest = new SignUpRequest(
                "Example",
                "example@test.com",
                Set.of("ROLE_USER"),
                "1234567890"
        );
        ResponseEntity actualResponse = authController.registerUser(signUpRequest);
        Assertions.assertThat(actualResponse.getStatusCodeValue()).isEqualTo(400);
    }

    @Test
    public void shouldRegisterNewUser(){
        SignUpRequest signUpRequest = new SignUpRequest(
                "Example",
                "example@test.com",
                Set.of("ROLE_USER"),
                "1234567890"
        );
        Mockito.when(userRepository.existsByUsername(Mockito.any(String.class)))
                .thenReturn(false);
        Mockito.when(userRepository.existsByEmail(Mockito.any(String.class)))
                .thenReturn(false);
        Mockito.when(encoder.encode(Mockito.any(String.class)))
                .thenReturn("xyz");
        Role r = new Role(ERole.ROLE_USER);
        r.setId("exampleId");

        Mockito.when(roleRepository.findByName(Mockito.any(ERole.class)))
                .thenReturn(Optional.of(r));
        Mockito.when(userRepository.save(Mockito.any(User.class)))
                .thenReturn(new User());
        ResponseEntity actualResponse = authController.registerUser(signUpRequest);
        Assertions.assertThat(actualResponse.getStatusCodeValue()).isEqualTo(200);
    }
}
