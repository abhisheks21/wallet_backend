package com.example.abhishek.WalletApp.controllers;

import com.example.abhishek.WalletApp.models.ERole;
import com.example.abhishek.WalletApp.models.Role;
import com.example.abhishek.WalletApp.models.Transaction;
import com.example.abhishek.WalletApp.models.User;
import com.example.abhishek.WalletApp.payload.response.MessageResponse;
import com.example.abhishek.WalletApp.repository.TransactionRepository;
import com.example.abhishek.WalletApp.repository.UserRepository;
import com.example.abhishek.WalletApp.security.services.UserDetailsImpl;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
@ContextConfiguration
class TransactionControllerTest {

    @Mock
    UserRepository userRepository;

    @Mock
    TransactionRepository transactionRepository;

    @InjectMocks
    TransactionController transactionController;
    @Mock
    Authentication authentication;
    @Mock
    SecurityContext securityContext;
    @Mock
    Transaction t;
//    @Mock
//    UserDetailsImpl userDetails;


    @Test
    @WithMockUser
    void topupWallet() {

        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User user = new User();
        user.setUsername("Example");
        user.setEmail("example@example.com");
        user.setWalletBalance(100);
        user.setPassword("1234567890");
        user.setRoles(set);

        UserDetailsImpl expectedUserObject = new UserDetailsImpl("12",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWalletBalance(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        Mockito.when(SecurityContextHolder.getContext()
                        .getAuthentication()
                        .getPrincipal())
                .thenReturn(expectedUserObject);
//        Mockito.when(userDetails.getId()).thenReturn("12");

        Mockito.when(transactionRepository.save(Mockito.any(Transaction.class)))
                .thenReturn(t);

        Mockito.when(userRepository.findById(Mockito.any(String.class))).thenReturn(Optional.of(user));
        Mockito.when(userRepository.save(Mockito.any(User.class)))
                .thenReturn(user);


        ResponseEntity<Map<String, Object>> actualResponse = transactionController.topupWallet(200);
//        assertEquals(actualResponse.toString(), 200, actualResponse.getStatusCode());
        Assertions.assertThat(actualResponse.getStatusCodeValue()).isEqualTo(200);
//        String actualMessage = (String) actualResponse.getBody().get("message");
//        Assertions.assertThat(actualMessage).isEqualTo("Wallet Recharge Successful!");
    }

    @Test
    void transferFund() {
        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User sender = new User();
        sender.setUsername("Example");
        sender.setEmail("example@example.com");
        sender.setWalletBalance(100);
        sender.setPassword("1234567890");
        sender.setRoles(set);

        Set<Role> set2 = Set.of(new Role(ERole.ROLE_USER));
        User receiver = new User();
        receiver.setUsername("ExampleReceiver");
        receiver.setEmail("example@receiver.com");
        receiver.setWalletBalance(150);
        receiver.setPassword("1234567890");
        receiver.setRoles(set2);


        UserDetailsImpl expectedUserObject = new UserDetailsImpl("12",
                sender.getUsername(),
                sender.getEmail(),
                sender.getPassword(),
                sender.getWalletBalance(),
                sender.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        Mockito.when(SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal())
                .thenReturn(expectedUserObject);
        Mockito.when(userRepository.findById(Mockito.any(String.class)))
                .thenReturn(Optional.of(sender));

        ResponseEntity<Map<String, Object>> actualResponse = transactionController
                .transferFund(200, "xyz");

        Assertions.assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        Mockito.when(userRepository.findById("empty")).thenReturn(Optional.empty());
        actualResponse = transactionController
                .transferFund(50, "empty");
        Assertions.assertThat(actualResponse.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);

        Mockito.when(userRepository.findById("xyz")).thenReturn(Optional.of(receiver));
        Mockito.when(userRepository.save(Mockito.any(User.class)))
                .thenReturn(sender);
        Mockito.when(transactionRepository.save(Mockito.any(Transaction.class)))
                .thenReturn(t);

        actualResponse = transactionController
                .transferFund(50, "xyz");
        Assertions.assertThat(actualResponse.getBody().get("accountBalance")).isEqualTo(50);
    }

    @Test
    void accountStatement() {
        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User user = new User();
        user.setUsername("Example");
        user.setEmail("example@example.com");
        user.setWalletBalance(100);
        user.setPassword("1234567890");
        user.setRoles(set);

        UserDetailsImpl expectedUserObject = new UserDetailsImpl("12",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWalletBalance(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));

        Transaction transaction1 = new Transaction("1","p1", "p2", 100,
                new java.util.Date(System.currentTimeMillis()));
        Transaction transaction2 = new Transaction("2","p3","p4", 450,
                new java.util.Date(System.currentTimeMillis()));
        Transaction transaction3 = new Transaction("3","p2","p3", 400,
                new java.util.Date(System.currentTimeMillis()));
        Transaction transaction4 = new Transaction("4","p1","p4",300,
                new java.util.Date(System.currentTimeMillis()));


        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        Mockito.when(SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal())
                .thenReturn(expectedUserObject);
//        Mockito.when(userDetails.getId()).thenReturn("12");

        Mockito.when(transactionRepository
                .findDistinctBySenderIdOrReceiverIdOrderByTimestampDesc(
                        Mockito.any(String.class),Mockito.any(String.class)))
                .thenReturn(List.of(transaction1, transaction2, transaction3, transaction4));

        Mockito.when(transactionRepository.findAllByReceiverId(Mockito.any(String.class)))
                .thenReturn(List.of(transaction3, transaction4));

        Mockito.when(transactionRepository.findAllBySenderId(Mockito.any(String.class)))
                .thenReturn(List.of(transaction1, transaction2));

        ResponseEntity<Map<String, Object>> actualResponse = transactionController
                .accountStatement();
//        assertEquals(actualResponse.toString(), 200, actualResponse.getStatusCode());
        Assertions.assertThat(actualResponse.getBody().get("accountBalance")).isEqualTo(150) ;
        Assertions.assertThat(actualResponse.getBody().get("transactions"))
                .isEqualTo(List.of(transaction1, transaction2, transaction3, transaction4));
        Assertions.assertThat(actualResponse.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void accountBalance(){
        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User user = new User();
        user.setUsername("Example");
        user.setEmail("example@example.com");
        user.setWalletBalance(100);
        user.setPassword("1234567890");
        user.setRoles(set);

        UserDetailsImpl expectedUserObject = new UserDetailsImpl("12",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWalletBalance(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));

        Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);

        Mockito.when(SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal())
                .thenReturn(expectedUserObject);

        Mockito.when(userRepository.findById(Mockito.any(String.class))).thenReturn(Optional.of(user));
        ResponseEntity actualResponse = transactionController
                .accountBalance();
        MessageResponse actualMessageBody = (MessageResponse) actualResponse.getBody();
        Assertions.assertThat(actualMessageBody.getMessage()).isEqualTo("100");
        Assertions.assertThat(actualResponse.getStatusCodeValue()).isEqualTo(200);

    }
}