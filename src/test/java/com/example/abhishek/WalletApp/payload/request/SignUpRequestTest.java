package com.example.abhishek.WalletApp.payload.request;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class SignUpRequestTest {

    SignUpRequest request;

    @BeforeEach
    void setup(){
        Set<String> s = Set.of("ROLE_USER");
        request = new SignUpRequest("Example", "example@example.com", s, "1234567890");
    }

    @Test
    void getUsername() {
        Assertions.assertThat(request.getUsername()).isEqualTo("Example");
    }

    @Test
    void setUsername() {
        request.setUsername("newUser");
        Assertions.assertThat(request.getUsername()).isEqualTo("newUser");

    }

    @Test
    void getEmail() {
        Assertions.assertThat(request.getEmail()).isEqualTo("example@example.com");

    }

    @Test
    void setEmail() {
        request.setEmail("newEmail@example.com");
        Assertions.assertThat(request.getEmail()).isEqualTo("newEmail@example.com");

    }

    @Test
    void getPassword() {
        Assertions.assertThat(request.getPassword()).isEqualTo("1234567890");

    }

    @Test
    void setPassword() {
        request.setPassword("password123");
        Assertions.assertThat(request.getPassword()).isEqualTo("password123");

    }

    @Test
    void getRoles() {
        Set<String> s = Set.of("ROLE_USER");
        Assertions.assertThat(request.getRoles()).isEqualTo(s);

    }

    @Test
    void setRole() {
        Set<String> s = Set.of("ROLE_USER", "ROLE_MOD");
        request.setRole(s);
        Assertions.assertThat(request.getRoles()).isEqualTo(s);

    }
}