package com.example.abhishek.WalletApp.payload.response;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class JwtResponseTest {

    JwtResponse response;

    @BeforeEach
    public void setup(){
        response = new JwtResponse("ExampleToken",
                "ExampleId",
                "ExampleUser",
                "example@example.com",
                1323,
                List.of("ROLE_USER"));
    }

    @Test
    void getWalletBalance() {
        Assertions.assertThat(response.getWalletBalance()).isEqualTo(1323);
    }

    @Test
    void setWalletBalance() {
        response.setWalletBalance(1222);
        Assertions.assertThat(response.getWalletBalance()).isEqualTo(1222);
    }

    @Test
    void getAccessToken() {
        Assertions.assertThat(response.getAccessToken()).isEqualTo("ExampleToken");
    }

    @Test
    void setAccessToken() {
        response.setAccessToken("NewToken");
        Assertions.assertThat(response.getAccessToken()).isEqualTo("NewToken");
    }

    @Test
    void getTokenType() {
        Assertions.assertThat(response.getTokenType()).isEqualTo("Bearer");
    }

    @Test
    void setTokenType() {
        response.setTokenType("Bear Token");
        Assertions.assertThat(response.getTokenType()).isEqualTo("Bear Token");
    }

    @Test
    void getId() {
        Assertions.assertThat(response.getId()).isEqualTo("ExampleId");
    }

    @Test
    void setId() {
        response.setId("NewExampleId");
        Assertions.assertThat(response.getId()).isEqualTo("NewExampleId");
    }

    @Test
    void getEmail() {
        Assertions.assertThat(response.getEmail()).isEqualTo("example@example.com");
    }

    @Test
    void setEmail() {
        response.setEmail("example2@example.com");
        Assertions.assertThat(response.getEmail()).isEqualTo("example2@example.com");
    }

    @Test
    void getUsername() {
        Assertions.assertThat(response.getUsername()).isEqualTo("ExampleUser");
    }

    @Test
    void setUsername() {
        response.setUsername("NewUser");
        Assertions.assertThat(response.getUsername()).isEqualTo("NewUser");
    }

    @Test
    void getRoles() {
        Assertions.assertThat(response.getRoles()).isEqualTo(List.of("ROLE_USER"));
    }
}