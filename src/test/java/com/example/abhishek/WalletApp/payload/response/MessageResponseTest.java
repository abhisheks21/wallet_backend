package com.example.abhishek.WalletApp.payload.response;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.junit.jupiter.api.Assertions.*;

class MessageResponseTest {

    @InjectMocks
    MessageResponse messageResponse = new MessageResponse("Example");

    @Test
    void getMessage() {
        messageResponse.setMessage("Example");
        Assertions.assertThat(messageResponse.getMessage()).isEqualTo("Example");
    }

    @Test
    void setMessage() {
        messageResponse.setMessage("NewExample");
        Assertions.assertThat(messageResponse.getMessage()).isEqualTo("NewExample");

    }
}