package com.example.abhishek.WalletApp.security.services;

import com.example.abhishek.WalletApp.models.ERole;
import com.example.abhishek.WalletApp.models.Role;
import com.example.abhishek.WalletApp.models.User;
import com.example.abhishek.WalletApp.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class UserDetailsServiceImplTest {

    @Mock
    UserRepository userRepository;
//            = Mockito.mock(UserRepository.class);

    @Mock
    UserDetailsImpl userDetails;
//    = Mockito.mock(UserDetailsImpl.class);

    @InjectMocks
    UserDetailsServiceImpl userDetailsService;

    @Test
    void loadUserByUsername() {
        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User user = new User();
        user.setUsername("Example");
        user.setEmail("example@example.com");
        user.setWalletBalance(100);
        user.setPassword("1234567890");
        user.setRoles(set);

        UserDetailsImpl expectedUserDetails = new UserDetailsImpl("12",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWalletBalance(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));
        Mockito.when(userRepository.findByUsername("Example")).thenReturn(Optional.of(user));

        MockedStatic<UserDetailsImpl> userDetailsMockedStatic = Mockito.mockStatic(UserDetailsImpl.class);
        userDetailsMockedStatic.when(() -> UserDetailsImpl.build(user)).thenReturn(expectedUserDetails);

        UserDetails actualResponse = userDetailsService.loadUserByUsername("Example");

        Assertions.assertThat(actualResponse.getUsername()).isEqualTo(expectedUserDetails.getUsername());
        Assertions.assertThat(actualResponse.getPassword()).isEqualTo(expectedUserDetails.getPassword());
    }
}