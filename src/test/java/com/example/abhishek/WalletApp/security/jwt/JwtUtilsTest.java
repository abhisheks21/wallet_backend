package com.example.abhishek.WalletApp.security.jwt;

import com.example.abhishek.WalletApp.models.ERole;
import com.example.abhishek.WalletApp.models.Role;
import com.example.abhishek.WalletApp.models.User;
import com.example.abhishek.WalletApp.security.services.UserDetailsImpl;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.assertj.core.api.Assertions;
import org.springframework.test.util.ReflectionTestUtils;
import java.util.Set;
import java.util.stream.Collectors;

@ExtendWith(MockitoExtension.class)
class JwtUtilsTest {

    @InjectMocks
    JwtUtils jwtUtils;

    @Mock
    Authentication authentication;

//    @Value("${WalletApp.app.jwtSecret}")
    private String jwtSecret = "thisismysecretforthejwtsecrettoken";

//    @Value("${WalletApp.app.jwtExpirationMs}")
    private int jwtExpirationMs = 8640000;

    @BeforeEach
    public void setup(){
        ReflectionTestUtils.setField(jwtUtils,
                "jwtSecret",
                "thisismysecretforthejwtsecrettoken" );

        ReflectionTestUtils.setField(jwtUtils,
                "jwtExpirationMs",
                8640000 );
    }

    @Test
//    @WithMockUser(username = "Example", roles = "ROLE_USER")
    void generateJwtToken() {

        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User user = new User();
        user.setUsername("Example");
        user.setEmail("example@example.com");
        user.setWalletBalance(100);
        user.setPassword("1234567890");
        user.setRoles(set);

        UserDetailsImpl expectedUserDetails = new UserDetailsImpl("12",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWalletBalance(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));

//        System.out.println(jwtSecret);
        Mockito.when(authentication.getPrincipal()).thenReturn(expectedUserDetails);

        Assertions.assertThat(jwtUtils.generateJwtToken(authentication)).isNotNull();
    }

    @Test
    void getUsernameFromJwtToken() {
        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User user = new User();
        user.setUsername("Example");
        user.setEmail("example@example.com");
        user.setWalletBalance(100);
        user.setPassword("1234567890");
        user.setRoles(set);

        UserDetailsImpl expectedUserDetails = new UserDetailsImpl("12",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWalletBalance(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));

//        System.out.println(jwtSecret);
        Mockito.when(authentication.getPrincipal()).thenReturn(expectedUserDetails);

        String token = jwtUtils.generateJwtToken(authentication);
        String actual_username = jwtUtils.getUsernameFromJwtToken(token);

        Assertions.assertThat(actual_username).isEqualTo("Example");
    }

    @Test
    void validateJwtToken() {
        Set<Role> set = Set.of(new Role(ERole.ROLE_USER));
        User user = new User();
        user.setUsername("Example");
        user.setEmail("example@example.com");
        user.setWalletBalance(100);
        user.setPassword("1234567890");
        user.setRoles(set);

        UserDetailsImpl expectedUserDetails = new UserDetailsImpl("12",
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getWalletBalance(),
                user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name()))
                        .collect(Collectors.toList()));

//        System.out.println(jwtSecret);
        Mockito.when(authentication.getPrincipal()).thenReturn(expectedUserDetails);

        String token = jwtUtils.generateJwtToken(authentication);

        Boolean result = jwtUtils.validateJwtToken(token);
        Assertions.assertThat(result).isEqualTo(true);
        Assertions.assertThat(jwtUtils.validateJwtToken("cdfdfvdf")).isEqualTo(false);

//        org.junit.jupiter.api.Assertions.assertThrows(SecurityException.class, ()-> jwtUtils.validateJwtToken("csdcs"));
    }
}