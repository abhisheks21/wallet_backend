package com.example.abhishek.WalletApp.security.jwt;

import com.example.abhishek.WalletApp.security.services.UserDetailsImpl;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.security.Key;
import java.util.Date;

@Component
public class JwtUtils {

    @Value("${WalletApp.app.jwtSecret}")
    private String jwtSecret;

    @Value("${WalletApp.app.jwtExpirationMs}")
    private int jwtExpirationMs;

    public  String generateJwtToken(Authentication authentication){
        UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();

        byte[] keyBytes = jwtSecret.getBytes();
        Key key = Keys.hmacShaKeyFor(keyBytes);
        return Jwts.builder()
                .setSubject((userPrincipal.getUsername()))
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
                .signWith(key,SignatureAlgorithm.HS256)
                .compact();
    }

    public String getUsernameFromJwtToken(String token){
        byte[] secretBytes = jwtSecret.getBytes();
        return Jwts.parserBuilder()
                .setSigningKey(secretBytes)
                .build()
                .parseClaimsJws(token)
                .getBody().getSubject();
    }

    public boolean validateJwtToken(String authToken){
        try{
            byte[] secretBytes = jwtSecret.getBytes();
//            System.out.println("Validating");
            Jwts.parserBuilder()
                    .setSigningKey(secretBytes)
                    .build()
                    .parseClaimsJws(authToken);
            return true;
        } catch(SecurityException e){
            System.err.println("Invalid JWT signature: " + e.getMessage() );
        } catch(MalformedJwtException e){
            System.err.println("Invalid JWT token: " + e.getMessage() );
        }catch(ExpiredJwtException e){
            System.err.println("JWT token is expired: " + e.getMessage() );
        }catch(UnsupportedJwtException e){
            System.err.println("JWT token is unsupported: " + e.getMessage() );
        }catch(IllegalArgumentException e){
            System.err.println("JWT claims string is empty: " + e.getMessage() );
        }
        return false;
    }
}
