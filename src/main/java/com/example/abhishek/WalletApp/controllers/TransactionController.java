package com.example.abhishek.WalletApp.controllers;

import com.example.abhishek.WalletApp.models.Transaction;
import com.example.abhishek.WalletApp.models.User;
import com.example.abhishek.WalletApp.payload.response.MessageResponse;
import com.example.abhishek.WalletApp.repository.TransactionRepository;
import com.example.abhishek.WalletApp.repository.UserRepository;
import com.example.abhishek.WalletApp.security.services.UserDetailsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.ok;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("api/transaction")
public class TransactionController {

        @Autowired
        TransactionRepository transactionRepository;

        @Autowired
        UserRepository userRepository;

        @PatchMapping(value = "/topup")
        @PreAuthorize("hasRole('USER')")
        public ResponseEntity<Map<String, Object>> topupWallet(@Valid @RequestParam("amount") int topupAmount){
                Map<String, Object> result = new HashMap<String, Object>();
                if(topupAmount <= 0){
                        result.put("accountBalance", "");
                        result.put("message", new MessageResponse("Donating is better " +
                                "than a negative recharge."));
                        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
                }
                UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder
                                                        .getContext()
                                                        .getAuthentication()
                                                        .getPrincipal();
                Transaction transaction = new Transaction();
                transaction.setSenderId("BANK");
                transaction.setReceiverId(userDetails.getId());
                transaction.setAmount(topupAmount);
                transaction.setTimestamp(new java.util.Date(System.currentTimeMillis()));

                transactionRepository.save(transaction);

                Optional<User> userOptional = userRepository.findById(userDetails.getId());



                if(userOptional.isPresent()){
                        User user = userOptional.get();
                        user.setWalletBalance(user.getWalletBalance()+topupAmount);
                        userRepository.save(user);

                        result.put("accountBalance", user.getWalletBalance());
                        result.put("message", new MessageResponse("Wallet Recharge Successful!"));
                        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
                } else {
                        result.put("accountBalance", "");
                        result.put("message", new MessageResponse("Wallet Recharge Failed!"));
                        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
                }
        }

        @PostMapping(value = "/transfer")
        @PreAuthorize("hasRole('USER')")
        public ResponseEntity<Map<String, Object>> transferFund(@Valid @RequestParam("transferAmount") int transferAmount,
                                              @RequestParam String receiverId){
                UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal();

                Optional<User> userOptional = userRepository.findById(userDetails.getId());
                if(userOptional.isPresent()){
                        User currentUser = userOptional.get();
                        if(currentUser.getWalletBalance() < transferAmount){
                                Map<String, Object> result = new HashMap<String, Object>();
                                result.put("accountBalance", currentUser.getWalletBalance());
                                result.put("message", new MessageResponse("Transaction failed. Insufficient account balance!"));
                                return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
                        }
                        Optional<User> receiverOptional = userRepository.findById(receiverId);
                        if(!receiverOptional.isPresent()){
                                Map<String, Object> result = new HashMap<String, Object>();
                                result.put("accountBalance", currentUser.getWalletBalance());
                                result.put("message", new MessageResponse("Transaction failed! Invalid receiver"));
                                return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
                        }
                        User receiverUser = receiverOptional.get();
                        receiverUser.setWalletBalance(receiverUser.getWalletBalance() + transferAmount);

                        currentUser.setWalletBalance(currentUser.getWalletBalance() - transferAmount);
                        userRepository.save(currentUser);
                        userRepository.save(receiverUser);

                        Transaction transaction = new Transaction();
                        transaction.setSenderId(userDetails.getId());
                        transaction.setReceiverId(receiverId);
                        transaction.setAmount(transferAmount);
                        transaction.setTimestamp(new java.util.Date(System.currentTimeMillis()));

                        transactionRepository.save(transaction);

                        Map<String, Object> result = new HashMap<String, Object>();
                        result.put("accountBalance", currentUser.getWalletBalance());
                        result.put("message", new MessageResponse("Transaction successfull!\n" +
                                " TransactionId:" + transaction.getTransactionId()));
                        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);

                }
                else {
                        Map<String, Object> result = new HashMap<String, Object>();
                        result.put("accountBalance", "");
                        result.put("message", new MessageResponse("Transaction failed! Invalid User"));
                        return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
                }

        }

        @GetMapping(value="/accountStatement")
        @PreAuthorize("hasRole('USER')")
        public ResponseEntity<Map<String, Object>> accountStatement(){
                // for account statement
                // search transaction database for userID either in sender or receiver
                // add amount in sender and add amount in receiver
                // deduct sender from receiver and calculate the account balance

                UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal();

                String userId = userDetails.getId();
                List<Transaction> accountTransactions =
                        transactionRepository
                                .findDistinctBySenderIdOrReceiverIdOrderByTimestampDesc(userId, userId);

                List<Transaction> sendingTransactions = transactionRepository.findAllBySenderId(userId);
                List<Transaction> receiverTransactions = transactionRepository.findAllByReceiverId(userId);

                int moneyOut = sendingTransactions
                                .stream()
                                .mapToInt(e -> e.getAmount())
                                .sum();
                int moneyIn = receiverTransactions
                                .stream()
                                .mapToInt(e -> e.getAmount())
                                .sum();
                int latestAccountBalance = moneyIn - moneyOut;

                Map<String, Object> result = new HashMap<String, Object>();
                result.put("accountBalance", latestAccountBalance);
                result.put("transactions", accountTransactions);
                return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
        }

        @GetMapping(value="/accountBalance")
        @PreAuthorize("hasRole('USER')")
        public ResponseEntity accountBalance(){
                UserDetailsImpl userDetails = (UserDetailsImpl) SecurityContextHolder.getContext()
                        .getAuthentication().getPrincipal();

                String userId = userDetails.getId();
                Optional<User> userOptional = userRepository.findById(userId);
                if(userOptional.isPresent()){
                        int balance = userOptional.get().getWalletBalance();
                        return new ResponseEntity(new MessageResponse(Integer.toString(balance)),
                                HttpStatus.OK);
                }else {
                        return new ResponseEntity(new MessageResponse("Invalid User!"),HttpStatus.BAD_REQUEST);
                }


        }

}
