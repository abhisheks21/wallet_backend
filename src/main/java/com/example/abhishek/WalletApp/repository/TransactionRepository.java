package com.example.abhishek.WalletApp.repository;

import com.example.abhishek.WalletApp.models.Transaction;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionRepository extends MongoRepository<Transaction, String> {
    List<Transaction> findAllBySenderId(String senderId);

    List<Transaction> findAllByReceiverId(String receiverId);

    List<Transaction> findAllBySenderIdAndReceiverIdOrderByTimestampDesc(String senderId, String receiverId);

    List<Transaction> findDistinctBySenderIdOrReceiverIdOrderByTimestampDesc(String senderId, String receiverId);

    Sort sort = Sort.by("timestamp").descending();
    List<Transaction> findDistinctBySenderIdOrReceiverId(String senderId, String receiverId, Sort sort);

}
